		
				
	// @param array de objetos
	// @param string property
	// @return array 
	function dim(array,prop){		
		var res = [];
			
		for (var i=0;i<array.length;i++)
			res.push(array[i][prop]);			
			
		return res;	
	}						
		
		
	// uso:
	// testTimming(function(){var x= 2/2020; });
	function testTimming(f){	
		var t0 = performance.now();
		f();
		var t1 = performance.now();
		console.log((t1 - t0) + " ms")
	}				
			