// Primera implementacion

class Bar
{		
	constructor (c=null,h=null,l=null,o=null,v=null,date=null){
		this.o = o;
		this.h = h;
		this.l = l;
		this.c = c;
		this.v = v;			
		this.date = date;
	}
}	


// Abstract class
class Indicator
{
	constructor(){
		this.data = [];
		this.result = null;
		
        if (this.constructor === Indicator) {
            throw new TypeError('Error: no puedes instanciar directamente la clase abstracta Indicador!'); 
        }		
		
		/* metodos abstractos */
		
		if (typeof this.calculate === "undefined") 
            throw new TypeError('Error: hay metodos sin implementar!');		
    }

	getResult(){
		return $this.result;
	}		
	
}	

// Acumula valores o sea suma desde i=0 hasta j, j<=N
class Sum extends Indicator
{	
	// recibe la propiedad del papel a operar
	constructor(property = 'c'){
		super();				
		this.prop = property;
	}
	
	add(val){		
		if (typeof val == "undefined" && val == null)
			throw new TypeError ("Sum no admite valores nulos!");
	
		// si son numeros
		if (!isNaN(parseFloat(val)) && isFinite(val))
			this.data.push(val);		
		else
			if (typeof val[this.prop] != 'undefined' ) 
				this.data.push(val[this.prop]);		
					else
						throw new TypeError ("Dato invalido en metodo add de Sum!");		
	}
	
	addRange(data){	
		for (var i=0; i<data[this.prop].length;i++)
			this.add(data[this.prop][i]);			
	}
	
	// cuantoas barras voy a afectar comenzando por la ultima?
	setN(bars){
		this.bars = bars;
	}
	
	calculate(){		
		var lastN= (typeof this.bars == 'undefined' || this.bars==null) ? this.data.length : this.bars;			
		this.result = 0;	
		for (var i=this.data.length-lastN;i<this.data.length;i++){			
			this.result += this.data[i];			
		}		
		
		return this.result;
	}	
	
	dump(){		
		console.info(this.data);
	}		
}	

	
class Chart
{
	constructor (data){
		this.indicators = [];
		this.data = [];
		this.data['o'] = [];
		this.data['c'] = [];
		this.data['h'] = [];
		this.data['l'] = [];
		this.data['v'] = [];
		this.data['date'] = [];		
				
		if (typeof data == "undefined" || !Array.isArray(data))						
			throw "Chart: argumento invalido!";	
		
		this.addRange(data);
	}		
	
	// ultimas N barras a mostrar con un posible desplazamiento hacia atras
	 setAxisX(bars=50, skip=0){
		this.range = [this.data.length -1 -bars -skip,this.data.length -1 -skip];
	 }			
		
	 addIndicator(ind){		 
		 this.indicators.push(ind);
	 }		
	 
	// recibe un objeto o un numero 
	// y lo agrega como dato
	add(val){			
		if (typeof val == "undefined" && val == null)
			throw new TypeError ("Dato invalido en metodo add de Chart!");

		if (val instanceof Bar || (val.o && val.c && val.h && val.l && val.v && val.date)){
			this.data.c.push(val.c);
			this.data.o.push(val.o);
			this.data.h.push(val.h);
			this.data.l.push(val.l);
			this.data.v.push(val.v);
			this.data.date.push(val.date);			
		}	
		else
			if (!isNaN(parseFloat(val)) && isFinite(val))
				this.data.c.push(val);
	}
	
	addRange(data)
	{
		if (typeof data == "undefined" || !Array.isArray(data))						
			throw "Chart: argumento invalido!";	
		
		// caso sean arrays de numeros en vez de barras
		if (typeof data[0] != "undefined" && typeof data[0] !== 'object' && typeof data[0].c == 'undefined'){
			for (var i=0; i<data.length; i++)
				if (!isNaN(parseFloat(data[i])) && isFinite(data[i]))
					this.data.c.push(data[i]);
			return;	
		}	
		
		// caso: son barras
		for (var i=0; i<data.length; i++)
			if (typeof data[i].c != 'undefined')
				this.data.c.push(data[i].c);
			else	
				this.data.c.push(null);												
					
		for (var i=0; i<data.length; i++)
			if (typeof data[i].o != 'undefined')
				this.data.o.push(data[i].o);								
			else	
				this.data.o.push(null);
					
		for (var i=0; i<data.length; i++)
			if (typeof data[i].l != 'undefined')
				this.data.l.push(data[i].l);									
			else	
				this.data.l.push(null);
					
		for (var i=0; i<data.length; i++)
			if (typeof data[i].h != 'undefined')
				this.data.h.push(data[i].h);										
			else	
				this.data.h.push(null);
					
		for (var i=0; i<data.length; i++)
			if (typeof data[i].v != 'undefined')
				this.data.v.push(data[i].v);										
			else	
				this.data.v.push(null);
				
		for (var i=0; i<data.length; i++)
			if (typeof data[i].date != 'undefined')
				this.data.date.push(data[i].date);									
			else	
				this.data.date.push(null);
	}
	
	calculate(){		
		for (var i=0; i<this.indicators.length; i++){												
			this.indicators[i].addRange(this.data);
			this.indicators[i].calculate();
			console.info (this.indicators[i].result);
		}	
	}
	
	// para debug
	dump(prop){
		for (var i=0; i<this.data[prop].length; i++)
			console.info(this.data[prop][i]);
	}		
	
}	

