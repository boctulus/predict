// "cte"
const NODATA = "NO DATA";

class Bar
{		
	constructor (c=null,h=null,l=null,o=null,v=null,date=null){
		this.o = o;
		this.h = h;
		this.l = l;
		this.c = c;
		this.v = v;			
		this.date = date;
	}
}	


// Abstract class
class PredictIndicator
{	
	init(){		
		throw new TypeError('Error: debe implementar init()');		
	}

	onStart(){				
	}
	
	onExit(){	
	}		
	
	calculate() {
		throw new TypeError('Error: debe implementar calculate()');		
	}
	
	// @return array de resultado
	get(){
		throw new TypeError('Error: debe implementar get()');		
	}	
	
	run(){		
		if (this.init() === false)
			return false;
			
		this.onStart();
		this.calculate();		
		this.onExit();
				
		if (this.result == null)
			throw "Null: el indicador no esta seteando el resultado";
		
		
		return this.result;
	};	

	constructor(data,params)
	{			
		
        if (this.constructor === Indicator) 
            throw new TypeError('Error: no se puede instanciar directamente la clase Indicador!'); 
        			
		if (!Array.isArray(data))
			throw new TypeError('Error: debe agregar datos al indicador de forma de objeto u array'); 								
		//if (typeof data == 'undefined' || data.length == 0)
		//	throw new TypeError('Error: no se encuentran datos en el indicador!'); 			
		
		this.data   = data;
		this.params = params;
	
		this.started = 0;
		this.index = 0;
		this.result = null;	
				
		if (typeof params != 'object' && isNumeric(params))
			this.n = params;
		else		
			this.params = params;							
    }	
	
	started(){
		return (this.started);
	}
	
	dump(){		
		console.info(this.data);
	}		
}	

// Abstracta
class Indicator extends PredictIndicator
{
	constructor(data,n)
	{
		super(data,n);
	}	
	
	init(){		
		var fail = ((this.data.length<this.n) || (this.index + this.n > this.data.length));
		this.started = !fail;
	
		if (fail)
			this.index++;	
		
		return this.started;
	}
	
	onExit(){
		this.index++;
	}		
	
	// retorna como arreglo
	get(){
		var res = [];	
		
		if (typeof this.n != 'undefined')
		{
			for (var ix=this.n; ix<=this.data.length; ix++){
				this.run();
				if (this.result!=NODATA)
					res.push(this.result);			
			}
		}else				
			// caso indicadores que no tuvieran periodo
			for (var ix=0; ix<this.data.length;ix++)
				res.push(this.run());			
		
		return res;
	}	

	
}


// Abstracta
class MultiParamIndicator extends PredictIndicator
{
	constructor(data,params)
	{
		super(data,params);
	}	
	
}


