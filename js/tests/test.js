var f = require('../indicators/functions.js');


var CLOSE, OPEN, HIGH, LOW, VOLUME;
		
CLOSE = [10.5,	11, 	11.5,	11,  	12,		11];
OPEN  = [10 , 	11, 	10.5,	12, 	12,		11.8];
HIGH  = [11 , 	11.5,	12,  	13,  	12.7,	11.8];
LOW   = [10 , 	11,	  	10.5,	10.5,	12,		11];		
VOLUME =[1000,	1800,	1950,	2100,	1920,	550];

 
//
//  AVA tests
//
//	https://github.com/avajs/ava  
//
import test from 'ava';


test(t => {
	t.true(f.sqrt([64])[0] == 8);
});

test(t => {
	t.deepEqual(f.cbrt([27,]),[3]);
});

test(t => {
	t.deepEqual(f.iif([true,false,true,false,false],VOLUME,f.mulScalar(VOLUME,-1)),  [1000, -1800, 1950, -2100, -1920]);	
});
 	 
	 
	   