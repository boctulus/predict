
class Chart
{
	constructor (data){
		this.indicators = [];
		this.data = [];
		this.data['o'] = [];
		this.data['c'] = [];
		this.data['h'] = [];
		this.data['l'] = [];
		this.data['v'] = [];
		this.data['date'] = [];		
				
		if (typeof data == "undefined" || !Array.isArray(data))						
			throw "Chart: argumento invalido!";	
		
		this.addRange(data);
	}		
	
	// ultimas N barras a mostrar con un posible desplazamiento hacia atras
	 setAxisX(bars=50, skip=0){
		this.range = [this.data.length -1 -bars -skip,this.data.length -1 -skip];
	 }			
		
	 addIndicator(ind){		 
		 this.indicators.push(ind);
	 }		
	 
	// recibe un objeto o un numero 
	// y lo agrega como dato
	add(val){			
		if (typeof val == "undefined" && val == null)
			throw new TypeError ("Dato invalido en metodo add de Chart!");

		if (val instanceof Bar || (val.o && val.c && val.h && val.l && val.v && val.date)){
			this.data.c.push(val.c);
			this.data.o.push(val.o);
			this.data.h.push(val.h);
			this.data.l.push(val.l);
			this.data.v.push(val.v);
			this.data.date.push(val.date);			
		}	
		else
			if (!isNaN(parseFloat(val)) && isFinite(val))
				this.data.c.push(val);
	}
	
	addRange(data)
	{
		if (typeof data == "undefined" || !Array.isArray(data))						
			throw "Chart: argumento invalido!";	
		
		// caso sean arrays de numeros en vez de barras
		if (typeof data[0] != "undefined" && typeof data[0] !== 'object' && typeof data[0].c == 'undefined'){
			for (var i=0; i<data.length; i++)
				if (!isNaN(parseFloat(data[i])) && isFinite(data[i]))
					this.data.c.push(data[i]);
			return;	
		}	
		
		// caso: son barras
		for (var prop of ['o','c','h','l','v','date'])						
			for (var i=0; i<data.length; i++)
				if (typeof data[i][prop] != 'undefined')
					this.data[prop].push(data[i][prop]);
				else	
					this.data[prop].push(null);																
	}
	
	calculate(){		
		for (var i=0; i<this.indicators.length; i++){												
			this.indicators[i].addRange(this.data);
			this.indicators[i].calculate();
			console.info (this.indicators[i].result);
		}	
	}
	
	// para debug
	dump(){
		console.info(this.data);
	}		
	
}	
