/*
	Indicadores varios
*/


class Sum extends Indicator
{
	constructor(data,n){
		super(data,n);
	}	
		
	calculate(){				
		// calculo n barras hacia atras
		var start = this.index+this.n-1;
		var end   = this.index-1;		
			
		if (this.result == null){
			this.result = 0;			
		
			for (var i=start;i>end;i--)
				this.result += this.data[i];							
			
		}else			
			this.result += this.data[this.index+this.n-1] - this.data[this.index-1];						
	}		
}



// ok 
class Momemtum extends Indicator
{
	constructor(data,n){
		super(data,n);
	}	
	
	calculate(){		
		this.result = this.data[this.index+this.n-1] - this.data[this.index];
	}
}	

// ok
class ROC extends Indicator
{
	constructor(data,n){
		super(data,n);		
	}	
	
	calculate(){
		// puede devolver Infinity
		this.result = (this.data[this.index+this.n-1] - this.data[this.index])/this.data[this.index];
	}
}	


// Codigo minimo de un indicador cualquiera que no hace uso de series de datos sino punto a punto
class HL extends Indicator
{
	constructor(data,n){		
		super(data,n);		
	}	

	calculate()
	{		
		this.result = this.data[this.index].h - this.data[this.index].l;				
	}			
}	

// ok
class Min extends Indicator
{
	constructor(data,n){		
		super(data,n);		
	}	
	
	// metodo auxiliar
	min(arr){
		var min =  Number.MAX_VALUE;		
						
		for (var i=0; i<arr.length; i++)
			if (arr[i] < min)
				min = arr[i];
			
		return min;	
	}	

	calculate()
	{			
		this.result = this.min(copy(this.data,this.index,this.index+this.n));					
	}			
}	

// ok
class Max extends Indicator
{
	constructor(data,n){		
		super(data,n);		
	}	

	// metodo auxiliar
	max(arr){
		var max =  Number.MIN_VALUE;		
						
		for (var i=0; i<arr.length; i++)
			if (arr[i] >max)
				max = arr[i];
			
		return max;	
	}		
	
	calculate()
	{		
		this.result = this.max(copy(this.data,this.index,this.index+this.n));					
	}			
}	

// ok
class SMA extends Indicator
{
	constructor(data,n){		
		super(data,n);		
		this.suma = new Sum(data,n);
	}	

	calculate(){
		this.result = this.suma.run()/this.n;
	}	
}


// ok!!
class EMA extends Indicator
{
	constructor(data,n){				
		super(data,n);
		this.k = 2 / (n + 1);	
	}	

	calculate(){		
		if (this.result == null){
			// seed:			
			var sma = new SMA(this.data, this.n);
			sma.run();
			this.result = sma.result;
			
		}	
		else{									
			// EMA [today] = (Price [today] x K) + (EMA [yesterday] x (1 – K))							
			this.result = (this.data[this.index+this.n-1] * this.k) + (this.result * (1- this.k));
		}	
		
	}		
}


// ok!!
class MACDLine extends MultiParamIndicator
{
	constructor(data,params){
		super(data,params);
		// creo las EMAs
		this.EMA12 = new EMA(data,params[0]);	
		this.EMA26 = new EMA(data,params[1]);
		
		// el EMA12 comienza primero asi que los valores quedan 
		// dsfazados y debo guardarlos en algun lado para poder 
		// calcular la diferencia (los alineo con estos datos)
		this.tmp = [];
	}
	
	init(){		
		// todas las EMAs deben ser inicializadas
		var ini_ema12 = this.EMA12.init();
		var ini_ema26 = this.EMA26.init();
	
		// la EMA mas larga comienza mas tarde
		// es la que determina cuando comienza el MACDLine
		return ini_ema12;
	}
	
	//	devuelve un arreglo
	get(){
		var res = [];	
		
		for (var ix=this.params[1]-1; ix<this.data.length+this.params[0]+2; ix++){
			this.run();
			if (this.result!=NODATA){
				res.push(this.result);
			}	
		}		
		return res;
	}	
	
	calculate(){			
		this.EMA26.run();
		this.EMA12.run();
			
		if (this.tmp.length < this.params[0]+2)
			this.result = NODATA;			
		else						
			this.result =  this.EMA12.result - this.tmp.shift();					
						
		this.tmp.push(this.EMA26.result);		
	}	
	
}

// SIN TERMINAR !!!
class MACDSignal extends MultiParamIndicator
{
	constructor(data,params){
		super(data,params);		
		this.MACD = new MACDLine(data,params);
		this.EMA9 = new EMA([],params[2]);	// conviene sea un SMA en principio
		this.tmp = [];
	}

	init(){		
		if (tmp.length == this.params[2]){
			//this.EMA9.data = ... tmp del MACD
			this.EMA9.init();	
		}			
		return this.MACD.ini();
	}
	
	calculate(){
		// .......
	}	
}	


class CountIf extends Indicator
{
	constructor(data,n,condition){				
		super(data,n);
		this.condition = condition;
	}	
	
	calculate(){
		this.result = countIf(copy(this.data,this.index,this.index+this.n),this.condition);		
	}
}

// Requerida para el RSI
class SumIf extends Indicator
{
	constructor(data,n,condition){				
		super(data,n);
		this.condition = condition;
	}	
	
	calculate(){
		this.result = sumIf(copy(this.data,this.index,this.index+this.n),this.condition);		
	}
}


