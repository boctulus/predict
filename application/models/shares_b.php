<?php
		
/**                                       	
* Crudo v0.1 - model generator for CodeIgniter 
* @author: Bozzolo Pablo (2011)
*/       
class Shares_B extends MY_Model { /// _M
    
	    const tb = 'shares';
	
        public function __construct()
	    {
		  parent::__construct();
        }
		
		
		/*
        * (C)reate
		*/
	    public function createShare($idShare=null,$symbol=null,$name=null,$idSymbolList=null)
		{
	      $ay = array('idShare'=>$idShare,'symbol'=>$symbol,'name'=>$name,'idSymbolList'=>$idSymbolList); 
	  	  
	      $this->db->insert(self::tb,$ay);
	      return $this->db->insert_id();
   	    }
		
		// Simple Insert: array $data -> shares
		public function insertShare($data)
        {      
          $this->db->insert(self::tb, $data);
		  return $this->db->insert_id();
        }
		
        /* 
		* D(elete)
		*/
	    public function deleteShare($idShare)
		{
	      $this->db->delete(self::tb, array('idShare'=>$idShare)); 	  
	      return $this->db->affected_rows();
	    }		
        
        /* 
		* D(elete) generico
		*/
	    public function shareDelete($where)
		{		  
		  
		  if (!empty($where))
		  {
		    if (is_array($where)){
		  
              foreach ($where as $key => $value){
	            $this->db->where($key,$value);
		       }					
			   
			}else{
			   $this->db->where($where);
            }			
			   
	      };					
	   
	      $this->db->delete(self::tb);	   
	      return $this->db->affected_rows();
	    }		
        					
		/* 		 
		*  R(read) : get() generico
		*/
	    public function shareGet($select=null,$where=null,$perpage=null,$start=0,$order=null,$joins=[],$as=null,$group_by=null)
		{ 			
		
		  // campos
		  if (!is_null($select)){		    
	        $this->db->select($select);  
	      }	          
		  
		  
		  if (!empty($where))
		  {
		    if (is_array($where)){
		  
              foreach ($where as $key => $value){
	            $this->db->where($key,$value);
		       }					
			   
			}else{
			   $this->db->where($where);
            }			
			   
	      };
		  
		  foreach ($joins as $join)
		  {
              foreach ($join as $key => $value){
	            $this->db->join($key,$value);
		      }					   
	      }
		  
		  if (!is_null($order)){
		    $order = "$order[0] $order[1]";
			$this->db->order_by($order);	        
	      }		
		  
		  if ($perpage!=null){		  		    
	        $this->db->limit($perpage,$start);			
		  }
		  
		  if (!is_null($group_by)){
		    $query = $this->db->group_by($group_by);
		  }		    
		  
		  if ($as == null){				 
	        $query = $this->db->get(self::tb);
		  }else{
		    $query = $this->db->get(self::tb.' AS '.$as); 
          }	
		  		  
	  
	      return $query->result();	  
	    }		
							
		/* 		 
		*  R(read) : exists ?
		*/
	    public function shareExists($where,$returnId=false)
		{	  
	      
		  if (!empty($where))
		  {
		    if (is_array($where)){
		  
              foreach ($where as $key => $value){
	            $this->db->where($key,$value);
		       }					
			   
			}else{
			   $this->db->where($where);
            }			
			   
	      };
		  
		  $query = $this->db->get(self::tb);     		 	
          $c_rows = ($query->num_rows());
		  
		  if (($returnId) AND ($c_rows==1)){
		    $r = $query->result();
			return $r[0]->idShare;
		  }
				
					
          return ($c_rows>=1);	  	  
	    }
				
		public function row(&$o){
		  if (is_array($o) && !empty($o)){
		    $o = $o[0];
		  }          
		}		
		
		
		/* 		 
		*  R(read) : get__ by Id
		*/
	    public function getIdShare($idShare,$where=null)
		{	  
		
		  $this->db->where('idShare',$idShare);     
	  			
		  
		  if (!empty($where))
		  {
		    if (is_array($where)){
		  
              foreach ($where as $key => $value){
	            $this->db->where($key,$value);
		       }					
			   
			}else{
			   $this->db->where($where);
            }			
			   
	      };			
			
          $query = $this->db->get(self::tb);	
		  
          if ($query->num_rows() > 0)
          {
             $row = $query->row();
             return $row->idShare;
          }else{
		    return null;
		  }
	    }
		
		/* 		 
		*  R(read) : get__ by Id
		*/
	    public function getSymbol($idShare,$where=null)
		{	  
		
		  $this->db->where('idShare',$idShare);     
	  			
		  
		  if (!empty($where))
		  {
		    if (is_array($where)){
		  
              foreach ($where as $key => $value){
	            $this->db->where($key,$value);
		       }					
			   
			}else{
			   $this->db->where($where);
            }			
			   
	      };			
			
          $query = $this->db->get(self::tb);	
		  
          if ($query->num_rows() > 0)
          {
             $row = $query->row();
             return $row->symbol;
          }else{
		    return null;
		  }
	    }
		
		/* 		 
		*  R(read) : get__ by Id
		*/
	    public function getName($idShare,$where=null)
		{	  
		
		  $this->db->where('idShare',$idShare);     
	  			
		  
		  if (!empty($where))
		  {
		    if (is_array($where)){
		  
              foreach ($where as $key => $value){
	            $this->db->where($key,$value);
		       }					
			   
			}else{
			   $this->db->where($where);
            }			
			   
	      };			
			
          $query = $this->db->get(self::tb);	
		  
          if ($query->num_rows() > 0)
          {
             $row = $query->row();
             return $row->name;
          }else{
		    return null;
		  }
	    }
		
		/* 		 
		*  R(read) : get__ by Id
		*/
	    public function getIdSymbolList($idShare,$where=null)
		{	  
		
		  $this->db->where('idShare',$idShare);     
	  			
		  
		  if (!empty($where))
		  {
		    if (is_array($where)){
		  
              foreach ($where as $key => $value){
	            $this->db->where($key,$value);
		       }					
			   
			}else{
			   $this->db->where($where);
            }			
			   
	      };			
			
          $query = $this->db->get(self::tb);	
		  
          if ($query->num_rows() > 0)
          {
             $row = $query->row();
             return $row->idSymbolList;
          }else{
		    return null;
		  }
	    }
		
		/* 		 
		*  U(update) : generico
		*
		*  $data  = array (clave,valor) de datos a actualizar
		*  $where = array (clave,valor) de condiciones 
		*/
		public function shareUpdate($data,$where)
		{		   
		  
		  if (!empty($where))
		  {
		    if (is_array($where)){
		  
              foreach ($where as $key => $value){
	            $this->db->where($key,$value);
		       }					
			   
			}else{
			   $this->db->where($where);
            }			
			   
	      };
		  
          $this->db->update(self::tb, $data); 
		  
		  return $this->db->affected_rows();
	    }
		
		/* 		 
		*  U(update) : update by id 
		*/
	    public function updateIdShare($idShare,$idShare)		
		{
	      $this->db->where('idShare',$idShare);   
          $this->db->update(self::tb,array('idShare'=>$idShare));
	  
	      return $this->db->affected_rows();
        }		
		  
		/* 		 
		*  U(update) : update by id 
		*/
	    public function updateSymbol($idShare,$symbol)		
		{
	      $this->db->where('idShare',$idShare);   
          $this->db->update(self::tb,array('symbol'=>$symbol));
	  
	      return $this->db->affected_rows();
        }		
		  
		/* 		 
		*  U(update) : update by id 
		*/
	    public function updateName($idShare,$name)		
		{
	      $this->db->where('idShare',$idShare);   
          $this->db->update(self::tb,array('name'=>$name));
	  
	      return $this->db->affected_rows();
        }		
		  
		/* 		 
		*  U(update) : update by id 
		*/
	    public function updateIdSymbolList($idShare,$idSymbolList)		
		{
	      $this->db->where('idShare',$idShare);   
          $this->db->update(self::tb,array('idSymbolList'=>$idSymbolList));
	  
	      return $this->db->affected_rows();
        }		
		  
} // end model class