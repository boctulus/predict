<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shares extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();  
		$this->load->model('shares_m','',TRUE);		
		$this->load->helper(['form', 'url', 'html','file', 'strings','array']);
		$this->load->library('view');
    }

    
    public function index()
    {           
        $this->listShares();
    }
		
	public function getShare($id)
	{          
        $where = ['idShare ='=>$id ];
		
		$r = $this->shares_m->shareGet(NULL,$where,NULL,NULL,NULL,[],NULL);
		echo json_encode($r);
	}
		
	public function listShares()
	{          
        $where = [ ];
		
		$r = $this->shares_m->shareGet(NULL,$where,NULL,NULL,NULL,[],NULL);
		echo json_encode($r);
	}
	
	
	
} // end class 