<form role="form" method="post" enctype="multipart/form-data" action="../productos/process">
    <div class="panel">
        <div class="panel-body">				          
            
            <div class="form-group">
				<label>Nombre del producto</label>
                <input class="form-control" type="text" name="NombreProducto" />
            </div>
			
			
			<div class="form-group">
				<label>Categoria(s)</label><p/>
				<?php
					echo form_multiselect('Categoria[]',$categorias);
				?>	
			</div>	
			<p/>
			
            <div class="form-group">
                <label>Descripcion</label><p/>
                <textarea class="form-control" type="text" name="DescripcionHTML" rows="4" cols="50" /></textarea>
            </div>
			
             <div class="form-group">
				<label>Precio</label>
                <input class="form-control" type="text" name="Valor" />
            </div>
			
			<div class="form-group">
				<label>Stock</label>
                <input class="form-control" type="text" name="Stock" />
            </div>
			
			<!-- podria incluir Marca -->
			<p/>
            <div class="form-group">
				<input name="imagenes[]" type="file" class="multi" accept="gif|jpg|png" multiple/>	
			</div>
			<p/>
			<input type="submit" />
		</div>	
    </div>
	
</form>